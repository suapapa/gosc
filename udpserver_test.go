package osc

import (
	// "fmt"
	"net"
	"testing"
	"time"
)

var msgs []*Message

func messageHandler1(m *Message) {
	msgs = append(msgs, m)
}

type OSCTestReceiver struct{}

func (r *OSCTestReceiver) HandleOSC(m *Message) {
	msgs = append(msgs, m)
}

func TestListenAndServe(t *testing.T) {

	recvr := new(OSCTestReceiver)

	// register handlers in both ways
	HandleFunc("/test1", messageHandler1)
	Handle("/test2", recvr)

	srv := &UDPServer{
		Address:     ":11111",
		Dispatcher:  nil,
		ReadTimeout: 250 * time.Millisecond,
	}

	// ensure we timeout as expected
	if err := srv.ListenAndServe(); !err.(net.Error).Timeout() {
		t.Error("ListenAndServeUDP:", err)
	}

	go srv.ListenAndServe()

	// Connect to server
	laddr, resolverr := net.ResolveUDPAddr("udp", srv.Address)
	if resolverr != nil {
		t.Error("ResolveUDPAddr:", resolverr)
	}

	conn, dialerr := net.DialUDP("udp", nil, laddr)
	if dialerr != nil {
		t.Error("DialUDP:", dialerr)
	}
	defer conn.Close()

	// Create OSC Message
	m1 := &Message{Address: "/test*"}
	m1.Args = append(m1.Args, float32(3.1415))

	// Write to server
	_, err := m1.WriteTo(conn)
	if err != nil {
		t.Errorf("Error: %s\n", err)
	}

	// give a little time for the message to be received & dispatched
	// then, wait for the server to timeout and shutdown
	time.Sleep(500 * time.Millisecond)

	if len(msgs) != 2 {
		t.Errorf("Wrong number of messages dispatched (%d), wanted %d", len(msgs), 2)
	}

	for _, m := range msgs {
		if !Equal(m1, m) {
			t.Error("received message didn't match:", m1, m)
		}
	}
}
