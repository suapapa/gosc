
# gosc

Pure Go (http://golang.org) support for OSC (http://opensoundcontrol.org)

Docs are available at [http://go.pkgdoc.org/bitbucket.org/liamstask/gosc](http://go.pkgdoc.org/bitbucket.org/liamstask/gosc)

## Install

`go get bitbucket.org/liamstask/gosc`


## Example
    :::go
    import (
	    "bitucket.org/liamstask/gosc"
        "net"
    )
	
    m := &osc.Message{Address: "/my/message"}
    m.Args = append(m.Args, int32(12345))
    m.Args = append(m.Args, "important")
	
    // error checking omitted for brevity...
	addr, _ := net.ResolveUDPAddr("udp", "127.0.0.1:12000")
	conn, _ := net.DialUDP("udp", nil, addr)

	numbytes, err := m.WriteTo(conn)
	if err != nil {
		// handle error
	}
	
See the tests for more detailed usage.

